echo "Start Building"  
#project_path="Unity-iPhone"  
project_path=$1
curdir=${project_path%/*}
echo curdir = $curdir
echo project_path = $project_path
UABUILD_ARCHIVES="/Users/iqigame/Documents/archives"
echo UABUILD_ARCHIVES = $UABUILD_ARCHIVES
UABUILD_IPA="/Users/iqigame/Documents/Ipa"
echo UABUILD_IPA = $UABUILD_IPA

plen=${#project_path}
prexx=${project_path:plen-1}
#echo prexx = $prexx
if [ "$prexx" == "/" ]; then
	#statements
	project_path=${project_path:0:plen-1}
	echo reset project_path = $project_path
fi
ipa_prefix=${project_path##*/}
echo ipa_prefix = $ipa_prefix

timey="`date +%Y%m%d`"
echo timey = $timey

timed="`date +%H%M%S`"
echo timed = $timed

ipa_name=${project_path##*/}
echo ipa_name = $ipa_name 
ipa_name=${ipa_name%%.*}
echo ipa_name = $ipa_name 

ipa_prefix=${ipa_name}
#export_plist=../${ipa_prefix}Args.plist
export_plist=../ExportOptionsDev.plist
echo export_plist = $export_plist 

project_name="Unity-iPhone.xcodeproj"  
configuration="Release"  

#archivePath=${UABUILD_ARCHIVES}/${ipa_name}.xcarchive  
archivePath=${project_path}
echo archivePath = $archivePath

ipaPath=${UABUILD_IPA}/Dev-${ipa_name}
ziptoPath=${ipaPath}.zip
echo ipaPath = $ipaPath
cd $project_path  
  
#echo "Clean Xcode"  
  
#xcodebuild clean | xcpretty | gnomon
#xcodebuild archive -project "${project_name}" -scheme "${scheme_name}" -configuration "$configuration" -archivePath "${archivePath}" -allowProvisioningUpdates | xcpretty | gnomon
echo archive to: $archivePath
xcodebuild -exportArchive -archivePath "${archivePath}" -exportOptionsPlist "${export_plist}" -exportPath "${ipaPath}" -allowProvisioningUpdates | xcpretty | gnomon
echo exportIPA to: $ipaPath
echo zip to:$ziptoPath
#zip -r ${ziptoPath} ${ipaPath}/* | gnomon
open ${ipaPath}  
  
echo "Successfully exported and signed the ipa file"  