echo "Start Building"  
#project_path="Unity-iPhone"  
project_path=$1
echo project_path = $project_path
plen=${#project_path}
prexx=${project_path:plen-1}
#echo prexx = $prexx
if [ "$prexx" == "/" ]; then
	#statements
	project_path=${project_path:0:plen-1}
	echo reset project_path = $project_path
fi

#UABUILD_ARCHIVES="/Users/iqigame/Documents/archives"
UABUILD_ARCHIVES="../../archives"
echo UABUILD_ARCHIVES = $UABUILD_ARCHIVES
#UABUILD_IPA="/Users/iqigame/Documents/Ipa"
UABUILD_IPA="../../ipa"
echo UABUILD_IPA = $UABUILD_IPA

ipa_prefix=${project_path##*/}
echo ipa_prefix = $ipa_prefix

scheme_name="Unity-iPhone"  

timey="`date +%Y%m%d`"
echo timey = $timey

timed="`date +%H%M%S`"
echo timed = $timed

ipa_name=${ipa_prefix}-${timey}-${timed}

#export_plist=../${ipa_prefix}Args.plist
#export_plistDev=../ExportOptionsDev.plist
#echo export_plistDev = $export_plistDev 

project_name="Unity-iPhone.xcodeproj"  
xcworkspace_name="Unity-iPhone.xcworkspace" 

configuration="Release"  

archivePath=${UABUILD_ARCHIVES}/${ipa_name}.xcarchive  
echo archivePath = $archivePath

cd $project_path  

if [ -f "podInstall.sh" ];then
	echo "podInstall.sh run"
	sh podInstall.sh
else
	echo "podInstall.sh not exit"
fi
  
#echo "Clean Xcode"  
  
#xcodebuild clean | xcpretty | gnomon
echo archive to: $archivePath
if [ -d "${xcworkspace_name}" ];then
	echo "${xcworkspace_name} exit use ${xcworkspace_name}"
	xcodebuild archive -workspace "${xcworkspace_name}" -scheme "${scheme_name}" -configuration "$configuration" -archivePath "${archivePath}" -allowProvisioningUpdates | xcpretty | gnomon

else
	echo "${xcworkspace_name} not exit use ${project_name}"
	xcodebuild archive -project "${project_name}" -scheme "${scheme_name}" -configuration "$configuration" -archivePath "${archivePath}" -allowProvisioningUpdates | xcpretty | gnomon

fi
#dis ipa
echo start dis ipa
ipaPath=${UABUILD_IPA}/${ipa_name}-Dis
echo exportIPA to: $ipaPath
export_plist=../../ExportOptionsDis.plist
echo export_plist = $export_plist 
ziptoPath=${ipaPath}.zip
echo ipaPath = $ipaPath
xcodebuild -exportArchive -archivePath "${archivePath}" -exportOptionsPlist "${export_plist}" -exportPath "${ipaPath}" -allowProvisioningUpdates | xcpretty | gnomon
echo complete die ipa
open ${ipaPath}  
#dev ipa
echo dev ipa
ipaPath=${UABUILD_IPA}/${ipa_name}-Dev
echo exportIPA to: $ipaPath
export_plist=../../ExportOptionsDev.plist
echo export_plist = $export_plist 
ziptoPath=${ipaPath}.zip
echo ipaPath = $ipaPath
xcodebuild -exportArchive -archivePath "${archivePath}" -exportOptionsPlist "${export_plist}" -exportPath "${ipaPath}" -allowProvisioningUpdates | xcpretty | gnomon
echo complete dev ipa
open ${ipaPath}  
#dev ipa
echo Adh ipa
ipaPath=${UABUILD_IPA}/${ipa_name}-Adh
echo exportIPA to: $ipaPath
export_plist=../../ExportOptionsAdh.plist
echo export_plist = $export_plist 
ziptoPath=${ipaPath}.zip
echo ipaPath = $ipaPath
xcodebuild -exportArchive -archivePath "${archivePath}" -exportOptionsPlist "${export_plist}" -exportPath "${ipaPath}" -allowProvisioningUpdates | xcpretty | gnomon
echo complete Adh ipa
open ${ipaPath}  
#cd $ipaPath
#zip -r ${ziptoPath} * | gnomon
#echo zip to:$ziptoPath
  
echo "Successfully exported and signed the ipa file"  